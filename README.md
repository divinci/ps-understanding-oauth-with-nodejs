# Understanding OAuth with Node.js

Project developed during the [Pluralsight - Understanding OAuth with Node.js](https://www.pluralsight.com/courses/understanding-oauth-with-nodejs) course.

Running the project:
1. Open the directory: `cd globomantics-carved-rock-example`.
 
2. Install dependencies: `npm install`.
 
3. Run the auth server: `node carvedRockAuthorizationServer`.
 
4. Run the resource owner: `node carvedRockProtectedResource`.
 
5. Run the client: `node globomanticsClient`.
